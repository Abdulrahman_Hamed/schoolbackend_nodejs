const express= require('express');

const Mongoose = require('mongoose');

const bodyParser=require('body-parser');
const app=express();

/* app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  }); */
Mongoose.connect('mongodb://localhost/ninjago');

Mongoose.Promise=global.Promise;
app.use(bodyParser.json());
app.use('/api',require('./routes/api'));

app.use(function(err,req,res,next){
    res.status(422).send({error : err.message});
});
app.listen(process.env.port || 4000,function(){
    console.log('now listining to port 4000');
});

