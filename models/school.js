const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;



const SchoolSchema = new Schema({

    name: {
        type: String,
        required: [true, 'Name is required']
    },
    address: {
        type: String,
        required: [true, 'Address is required']
    },
    type: {
        type: String,
        required: [true, 'Type is required']
    }

});

const School = Mongoose.model('school', SchoolSchema);

module.exports = School;