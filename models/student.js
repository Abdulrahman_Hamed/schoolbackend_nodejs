const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;



const StudentSchema = new Schema({

    name: {
        type: String,
        required: [true, 'Name is required']
    },
    address: {
        type: String,
        required: [true, 'Address is required']
    },
    age: {
        type: Number,
        required: [true, 'Age is required']
    },
    gender: {
        type: String,
        required: [true, 'Gender is required']
    }

});

const Student = Mongoose.model('student', StudentSchema);

module.exports = Student;