const express = require('express');
const School = require('../models/school');
const Student = require('../models/student')
const router = express.Router();

router.get('/schools', function (req, res, next) {
   School.find({}).then(function(schools){
       res.send(schools);
   })
});
router.post('/schools', function (req, res, next) {
    School.create(req.body).then(function (school) {
        res.send(school);
    }).catch(next);

});
router.put('/schools/:id', function (req, res, next) {
    School.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function () {
        School.findOne({_id:req.params.id}).then(function(school){
            res.send(school);
        })
    });

});
router.delete('/schools/:id', function (req, res, next) {
    console.log(req.params.id);
    School.findByIdAndRemove({ _id: req.params.id }).then(function (school) {
        res.send(school);
    });

});

// Students API
router.get('/students', function (req, res, next) {
    Student.find({}).then(function(students){
        res.send(students);
    })
});
router.post('/students', function (req, res, next) {
    Student.create(req.body).then(function (student) {
        res.send(student);
    }).catch(next);

});
router.put('/students/:id', function (req, res, next) {
    Student.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function () {
        Student.findOne({_id:req.params.id}).then(function(student){
            res.send(student);
        })
    });

});
router.delete('/students/:id', function (req, res, next) {
    Student.findByIdAndRemove({ _id: req.params.id }).then(function (student) {
        res.send(student);
    });

});

module.exports = router;